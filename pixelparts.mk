#$(call inherit-product, vendor/google/pixelparts/touch/device.mk)

# Camera
PRODUCT_BROKEN_VERIFY_USES_LIBRARIES := true
PRODUCT_PACKAGES += \
    GoogleCamera

# Parts
PRODUCT_PACKAGES += \
    GoogleParts \
    PixelFrameworksOverlay
